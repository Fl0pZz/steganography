defmodule Server.Plug.Router do
  use Plug.Router

  plug Plug.Logger, log: :debug
  plug Plug.Static, at: "/", from: "priv/static_files"
  plug Plug.Parsers, parsers: [:urlencoded, :multipart]

  plug :match
  plug :dispatch

  get "/", do: send_file(conn, 200, "priv/static_files/index.html")
  get "/img" do
    File.rm("priv/static_files/out.bmp")
    # conn = Plug.Conn.fetch_query_params(conn)
    query_params = conn |> Plug.Conn.fetch_query_params
    text = query_params.query_params["message"]
    Modifier.injector("1.bmp", "priv/static_files/out.bmp", text)
    send_file(conn, 200, "priv/static_files/out.bmp")
  end
  put "/addimg" do
    IO.inspect "put method"
    IO.inspect conn
    send_resp(conn, 200, "OK")
  end
  delete "/delimg/:name" do
    File.rm("priv/static_files/#{name}")
    IO.inspect name
    send_resp(conn, 200, "OK")
  end
  match _, do: send_resp(conn, 404, "Oops!")
end
