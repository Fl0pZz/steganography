defmodule Modifier do
  def injector(filein, fileout, text) do
    { max_capacity, img } = Bmp.read(filein)
    message = Message.create(text, max_capacity)
    bmp = Bmp.inject(message, img)
    Bmp.write(fileout, bmp)
  end

  def extracter(filein) do
    { _, img } = Bmp.read(filein)
    message = Bmp.extract(img)
    { :ok, message.message }
  end
end
