defmodule Message do
  defstruct size: 0, message: ""

  def create(text, max_byte_size) do
    graphemes = text
      |> String.graphemes
    { size, index } = Enum.reduce_while(graphemes, { 0, 0 }, fn i, acc ->
      { size, index } = acc
      if (size + byte_size(i) < max_byte_size) do
        { :cont, { size + byte_size(i), index + 1 } }
      else
        { :halt, { size, index } }
      end
    end)
    text = String.slice(text, 0, index)
    %Message{ size: size, message: text }
  end
end
