defmodule Bmp do
  use Bitwise
  defstruct [:pixels, :header]
  def read(filename) do
    {:ok, bindata} = File.read(filename)
    <<  "BM",
      _::size(64),
      offset_to_pixels:: unsigned-little-integer-size(32),
      _::size(32),
      width::size(32)-little,
      height::size(32)-little,
      _::size(16),
      24::size(16)-little,
      _rest::binary>> = bindata
    <<header::size(offset_to_pixels)-bytes, pixels::binary>> = bindata
    { width * height * 3 / 8, %Bmp{ pixels: pixels, header: header } }
  end

  def _inject_bit(<<>>, tail_pix, acc), do: { acc, tail_pix }

  def _inject_bit(<< data_bit :: size(1), data_tail :: bitstring >>,
                  << color :: size(7), _ :: size(1), tail_pix :: binary >>, acc)
  do
    _inject_bit(data_tail, tail_pix, acc <> << color :: size(7), data_bit :: size(1) >>)
  end

  def inject(message, file) do
    { size_binary, pixels_tail } = _inject_bit(<< message.size :: unsigned-little-integer-size(32) >>, file.pixels, "")
    { message_binary, pixels_tail } = _inject_bit(message.message, pixels_tail, "")
    %Bmp{ header: file.header, pixels: size_binary <> message_binary <> pixels_tail }
  end

  def write(filename, data) do
    {:ok, file} = File.open filename, [:write]
    IO.binwrite file, data.header <> data.pixels
    File.close file
  end

  def _extract_bit(0, pixels_tail, acc), do: { acc, pixels_tail }

  def _extract_bit(count_bits, << byte, pixels_tail :: binary >>, acc) do
    << _ :: size(7), data_bit :: size(1) >> = << byte >>
    _extract_bit(count_bits - 1, pixels_tail, << acc :: bitstring, data_bit :: size(1) >>)
  end

  def extract(file) do
    { << size :: unsigned-little-integer-size(32) >>, pixels_tail } = _extract_bit(32, file.pixels, "")
    { message, _ } = _extract_bit(size * 8, pixels_tail, "")
    %Message{ size: size, message: message }
  end
end
