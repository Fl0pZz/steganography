## Installation
```bash
mix deps.get --all
iex -S mix
```
Как использовать либу стеганографии:
```bash
# Упаковка
iex> Modifier.injector("path/to/img", "path/to/outImg", your_text)

# извлечение
iex> Modifier.extracter("path/to/img")
```

Как запускатб сервер:
```bash
mix deps.compile
mix run --no-halt
```
